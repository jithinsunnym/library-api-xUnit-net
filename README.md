# library-api-xUnit.net
Create a sample Library API and perform Unit Test at all API endpoint using xUnit.net Framework.


dotnet tool install -g dotnet-reportgenerator-globaltool

reportgenerator -reports:"D:\GitLab\library-api-xUnit-net\LibraryAPI.Test\TestResults\a18f8448-c9d3-42fd-a4aa-7a283cadf850\coverage.cobertura.xml" -targetdir:"coveragereport" -reporttypes:Html
